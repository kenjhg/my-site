import _ from 'lodash'
import React, { Component } from 'react'
import CustomNavItem from './CustomNavItem'
import FontAwesome from 'react-fontawesome'
import { PropTypes } from 'prop-types'

const nav = [
  { title: 'About', path: '/', exact: true },
  { title: 'Skills', path: '/skills' },
  { title: 'Projects', path: '/projects' },
]

export default class NavBar extends Component {

  static defaultProps = {
    navStyle: 'menu',
  }

  static propTypes = {
    navStyle: PropTypes.string.isRequired,
  }

  render() {
    return (
      <nav className={this.props.navStyle}>
        <div className="nav-list">
          {_.map(nav, item => (
            <CustomNavItem key={item.title} item={item} />
          ))}
        </div>
        <div className="nav-link">
          <div>Get in touch</div>
          <a target="_blank" rel="noopener noreferrer" href="https://www.linkedin.com/in/ken-j-huang/">
            <FontAwesome name="linkedin" />
          </a>
          <a target="_blank" rel="noopener noreferrer" href="https://github.com/kenhg">
            <FontAwesome name="github" />
          </a>
          <a target="_blank" rel="noopener noreferrer" href="mailto:ken.j.hg@gmail.com">
            <FontAwesome name="envelope-o" />
          </a>
        </div>
      </nav>
    )
  }
}
