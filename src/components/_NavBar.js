import _ from 'lodash'
import React, { Component } from 'react'
import { Navbar, NavbarItem, NavbarMenu, NavbarStart, NavbarEnd, NavbarBrand, NavbarBurger } from 'bloomer'
import FontAwesome from 'react-fontawesome'
import CustomNavItem from './CustomNavItem'

const nav = [
  { icon: 'home', title: 'Home', path: '/', exact: true },
  // { icon: 'user-o', title: 'About', path: '/about' },
  { icon: 'gears', title: 'Skills', path: '/skills' },
  { icon: 'code', title: 'Projects', path: '/projects' },
]

export default class NavBar extends Component {

  state = { isActive: false }

  onClickNav = () => {
    this.setState({ isActive: !this.state.isActive })
  }

  render() {
    return (
      <div className="main-nav">
        <Navbar className="is-primary">
          <NavbarBrand>
            <NavbarItem target="_blank" rel="noopener noreferrer" href="https://www.linkedin.com/in/ken-j-huang/" isHidden="desktop">
              <FontAwesome name="linkedin" />
            </NavbarItem>
            <NavbarItem target="_blank" rel="noopener noreferrer" href="https://github.com/kenhg" isHidden="desktop">
              <FontAwesome name="github" />
            </NavbarItem>
            <NavbarItem href="mailto:ken.j.hg@gmail.com" isHidden="desktop">
              <FontAwesome name="envelope-o" />
            </NavbarItem>
            <NavbarBurger isActive={this.state.isActive} onClick={this.onClickNav} />
          </NavbarBrand>
          <NavbarMenu isActive={this.state.isActive} onClick={this.onClickNav}>
            <NavbarStart>
              {_.map(nav, (item, i) => (
                <CustomNavItem key={i} item={item} />
              ))}
            </NavbarStart>
            <NavbarEnd>
              <NavbarItem target="_blank" rel="noopener noreferrer" href="https://www.linkedin.com/in/ken-j-huang/" isHidden="touch">
                <FontAwesome name="linkedin" />
              </NavbarItem>
              <NavbarItem target="_blank" rel="noopener noreferrer" href="https://github.com/kenhg" isHidden="touch">
                <FontAwesome name="github" />
              </NavbarItem>
              <NavbarItem href="mailto:ken.j.hg@gmail.com" isHidden="touch">
                <FontAwesome name="envelope-o" />
              </NavbarItem>
            </NavbarEnd>
          </NavbarMenu>
        </Navbar>
      </div>
    )
  }
}
