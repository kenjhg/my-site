import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Home from './Home'
import Projects from './Projects'
import ProjectDetail from './ProjectDetail'
import Skills from './Skills'
import NavBar from './NavBar'

const Main = () => (
  <div className="main">
    <NavBar navStyle="bar" />
    <Switch>
      <Route exact path="/" component={Home} />
      <Route exact path="/projects" component={Projects} />
      <Route exact path="/projects/:slug" component={ProjectDetail} />
      <Route exact path="/skills" component={Skills} />
    </Switch>
  </div>
)

export default Main
