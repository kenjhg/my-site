import React from 'react'
import { Link, Route } from 'react-router-dom'
import { PropTypes } from 'prop-types'

const CustomNavItem = ({ item }) => (
  <Route
    path={item.path}
    exact={item.exact}
    children={({ match }) => { //eslint-disable-line
      const isActive = match
      return (
        <div className="nav-item">
          <Link to={item.path} className={isActive ? ' active' : ''}>
            {item.title}
          </Link>
        </div>
      )
    }}
  />
)

CustomNavItem.propTypes = {
  item: PropTypes.object.isRequired,
}

export default CustomNavItem
