import _ from 'lodash' // eslint-disable-line
import React, { Component } from 'react'
import { PropTypes } from 'prop-types'
import projects from '../consts/projects'

export default class Project extends Component {

  static propTypes = {
    match: PropTypes.object.isRequired,
  }

  constructor(props) {
    super(props)
    this.state = {
      project: projects.hasOwnProperty(props.match.params.slug) ? projects[props.match.params.slug] : null,
    }
  }
  title = () => {
    const { project: { link, title } } = this.state
    const titleEle = link ? (<a href={link} target="_blank" rel="noreferrer noopener">{title}</a>) : title

    return (<h2 className="project-title">{titleEle}</h2>)
  }

  image = () => {
    const { project: { imageLink, link, title } } = this.state
    const imageEle = <img src={imageLink} style={{ width: '100%' }} alt={title} />

    if (link) return (<a href={link} target="_blank" rel="noreferrer noopener">{imageEle}</a>)
    return imageEle
  }

  description = () => {
    const { project: { description, html } } = this.state

    if (html) return <p className="project-description" dangerouslySetInnerHTML={{ __html: html }} />

    return <p className="project-description">{description}</p>
  }

  render() {
    const { project } = this.state

    if (!project) return <h1>Project Not Found</h1>

    return (
      <div className="project-detail">
        {this.title()}
        <div className="project-image" style={{ maxWidth: 700 }}>
          {this.image()}
        </div>
        {this.description()}
      </div>
    )
  }
}
