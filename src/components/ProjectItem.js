import _ from 'lodash'
import React, { Component } from 'react'
import { PropTypes } from 'prop-types'

export default class Project extends Component {

  static propTypes = {
    project: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
    slug: PropTypes.string.isRequired,
  }

  state = {
    hovered: false,
  }

  onActive = () => this.setState({ hovered: true })

  onInactive = () => this.setState({ hovered: false })

  title = () => {
    const { project: { link, title } } = this.props
    const titleEle = link ? (<a href={link} target="_blank" rel="noreferrer noopener">{title}</a>) : title

    return (<h2 className="project-title">{titleEle}</h2>)
  }

  image = () => {
    const { project: { imageLink, link, title } } = this.props
    const imageEle = <img src={imageLink} alt={title} />

    if (link) return (<a href={link} target="_blank" rel="noreferrer noopener">{imageEle}</a>)
    return imageEle
  }

  description = () => {
    const { project: { description, html } } = this.props

    if (html) return <p className="project-description" dangerouslySetInnerHTML={{ __html: html }} />

    return <p className="project-description">{description}</p>
  }

  render() {
    const { project: { tags }, history, slug } = this.props
    const { hovered } = this.state
    return (
      // <di
      <div className={`project-item${hovered ? ' hovered' : ''}`}>
        <div className="project-image" onMouseEnter={this.onActive} onMouseLeave={this.onInactive}>
          {this.image()}
          <div className="tag-list" onClick={() => history.push(`/projects/${slug}`)}>
            <div className="tags">{_.map(tags, (tag, i) => <span key={i} className="tag">{tag}</span>)}</div>
            <div className="info"><span className="tag small">click for more...</span></div>
          </div>
        </div>
      </div>
    )
  }
}
