import React from 'react'
import { Link } from 'react-router-dom'

export default () => (
  <dev className="home">
    <h1 className="title">Hi, I'm Ken and I'm a web developer.</h1>
    <p>
      I am specialised in both front-end and back-end web development. You can check out my <Link to="/skills">skills</Link> and <Link to="/projects">projects</Link> I have worked/currently working on.
    </p>
    <p>I am currently working at <a target="_blank" rel="noopener noreferrer" href="https://1scope.com">1Scope</a>.</p>
  </dev>
)
