import _ from 'lodash'
import React, { Component } from 'react'
import SkillItem from './SkillItem'
import skills from '../consts/skills'

export default class Skills extends Component {
  render() {
    return (
      <div className="skills">
        <div>
          {_.map(skills, (skill, i) => <SkillItem key={i} skill={skill} />)}
        </div>
      </div>
    )
  }
}
