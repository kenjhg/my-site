import React, { Component } from 'react'
import { Container, Columns, Column, Section } from 'bloomer'

export default class About extends Component {
  render() {
    return (
      <Section className="about">
        <Container>
          <Columns isCentered>
            <Column>
              <h1>About me</h1>
            </Column>
          </Columns>
          <Columns isCentered>
            <Column>
            </Column>
          </Columns>
        </Container>
      </Section>
    )
  }
}
