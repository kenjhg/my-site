import _ from 'lodash'
import React, { Component } from 'react'
import { Columns, Column, Tag } from 'bloomer'

export default class Project extends Component {
  render() {
    const { skill: { category, list } } = this.props
    return (
      <div className="skill-item">
        <Columns>
          <Column>
            <h2>{category}</h2>
          </Column>
        </Columns>
        <Columns>
          <Column>
            {_.map(list, (skill, i) => <Tag key={i} isColor="light" isSize="large">{skill.label}</Tag>)}
          </Column>
        </Columns>
      </div>
    )
  }
}
