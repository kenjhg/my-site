import _ from 'lodash'
import React, { Component } from 'react'
import { PropTypes } from 'prop-types'
import ProjectItem from './ProjectItem'
import projects from '../consts/projects'

export default class Projects extends Component {

  static propTypes = {
    history: PropTypes.object.isRequired,
  }

  render() {
    return (
      <div>
        <h1>Projects</h1>
        <div className="projects">
          {_.map(projects, (project, i) => <ProjectItem key={project.title} slug={i} history={this.props.history} project={project} />)}
        </div>
      </div>
    )
  }
}
