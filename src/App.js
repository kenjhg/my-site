import React, { Component } from 'react'
import './sass/app/App.css'
import NavBar from './components/NavBar'
import Main from './components/Main'

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="nav-container">
          <NavBar />
        </div>
        <div className="main-container">
          <Main />
        </div>
      </div>
    )
  }
}

export default App
