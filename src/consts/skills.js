export default [
  {
    category: 'Web Development',
    list: [
      { label: 'Node.js' },
      { label: 'React.js' },
      { label: 'React Native' },
      { label: 'Redux' },
      { label: 'Laravel' },
      { label: 'Jest/Jasmine' },
      { label: 'Webpack' },
      { label: 'Vue.js' },
      { label: 'Responsive Web Design' },
      { label: 'SASS' },
      { label: 'HTML5' },
      { label: 'CSS3' },
      { label: 'jQuery' },
    ],
  },
  {
    category: 'Programming Languages',
    list: [
      { label: 'Javascript' },
      { label: 'PHP' },
      { label: 'C#' },
    ],
  },
  {
    category: 'Databases',
    list: [
      { label: 'PostgreSQL' },
      { label: 'Redis' },
      { label: 'MongoDB' },
    ],
  },
  {
    category: 'Other',
    list: [
      { label: 'Amazon Web Services (AWS)' },
      { label: 'Git' },
      { label: 'Docker' },
      { label: 'Bitbucket Pipelines' },
      { label: 'JIRA' },
      { label: 'Agile Development' },
    ],
  },
]
