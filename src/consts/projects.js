export default {
  '1scope': {
    imageLink:
      'https://s3-ap-southeast-2.amazonaws.com/ken-huang.me/images/project-1scope.jpg',
    tags: [
      'React',
      'Redux',
      'Node.js',
      'PostgreSQL',
      'Redis',
      'AWS',
    ],
    title: '1Scope',
    description: '1Scope is an online platform that connects students (aged 12-25) with all opportunities available and allows them to categorically research, apply for, and evaluate opportunities.',
    link: 'https://1scope.com',
  },
  hap: {
    imageLink:
      'https://s3-ap-southeast-2.amazonaws.com/ken-huang.me/images/project-hap.jpg',
    tags: ['Vue.js', 'PHP Laravel', 'PostgreSQL', 'Heroku'],
    title: 'Health Advice Plus',
    description: 'Health Advice Plus is a service offered by the Pharmacy Guild of Australia, which is designed to to identify the earning capacity of Australian pharmacies and provide the support required for them to secure that income.',
    link: 'https://www.healthadviceplus.com.au/',
  },
  // {
  //   imageLink:
  //     'https://s3-ap-southeast-2.amazonaws.com/ken-huang.me/images/project-placeholder.jpg',
  //   tags: ['React', 'Redux', 'Telegram Bot API'],
  //   title: 'Telegram Chat Bot - Todo List',
  //   html: 'A simple To-do List web app, which is also integrated with the <a href="https://core.telegram.org/bots/api" target="_blank" rel="noreferrer noopener">Telegram Bot API</a> allowing usage through the Telegram application ',
  // },
  lc: {
    imageLink:
      'https://s3-ap-southeast-2.amazonaws.com/ken-huang.me/images/project-lc.jpg',
    tags: ['Wordpress', 'WooCommerce'],
    title: 'Life Compounding',
    link: 'https://lifecompounding.com.au/',
    description: 'Life Compounding is an e-Commerce website built on WordPress and WooCommerce for a compounding pharmacy based in Orange, NSW.',
  },
}
